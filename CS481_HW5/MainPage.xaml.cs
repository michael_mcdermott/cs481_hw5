﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace CS481_HW5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        Dictionary<string, Pin> pickerLocations = new Dictionary<string, Pin>
        {
            { "High School", new Pin
                {
                    Position = new Position(33.5642, -117.2293),
                    Label = "High School",
                    Type = PinType.Place
                }
            },
            { "Peaceful Place", new Pin
                {
                    Position = new Position(33.0368, -117.2951),
                    Label = "Peaceful Place",
                    Type = PinType.Place
                }
            },
            { "Favorite Brewery", new Pin
                {
                    Position = new Position(33.2156,-117.2669),
                    Label = "Favorite Brewery",
                    Type = PinType.Place
                }
            },
             { "Work", new Pin
                {
                    Position = new Position(33.0713,-117.2651),
                    Label = "Work",
                    Type = PinType.Place
                }
            }
        };

       

public MainPage()
        {
            InitializeComponent();
            var initialMapLocation = MapSpan.FromCenterAndRadius(new Position(33.1307785,-117.1601826),Distance.FromMiles(1));
            myMap.MoveToRegion(initialMapLocation);

            foreach (Pin pin in pickerLocations.Values)
            {
                myPicker.Items.Add(pin.Label);
                myMap.Pins.Add(pin);
            }
  
            myPicker.SelectedIndexChanged += (sender, args) =>
            {
                if (myPicker.SelectedIndex == -1)
                {
                
                }
                else
                {
                    string location = myPicker.Items[myPicker.SelectedIndex];
                    var updatedLocation = pickerLocations[location].Position;
                    myMap.MoveToRegion(MapSpan.FromCenterAndRadius(updatedLocation, Distance.FromMiles(.25)));

                }
            };
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            var button = (Button)sender;
            switch (button.Text)
            {
                case "Satellite":
                    myMap.MapType = MapType.Satellite;
                    break;
                case "Street":
                    myMap.MapType = MapType.Street;
                    break;

                case "Hybrid":
                    myMap.MapType = MapType.Hybrid;
                    break;
                default:
                    break;
            }

        }


    }
}
